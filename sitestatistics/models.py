from django.db import models
from django.utils.translation import ugettext_lazy as _


class Statistic(models.Model):
    site = models.ForeignKey('siteeditor.Site', verbose_name=_('Site'))
    ip = models.GenericIPAddressField(verbose_name='IP')
    time_on_site = models.DurationField(verbose_name=_('Time on site'))
    date = models.DateTimeField(verbose_name=_('Time'), auto_now_add=True)
    source = models.URLField(verbose_name=_('Source'))
    entrance_page = models.ForeignKey('siteeditor.Block', verbose_name=_('Entrance page'),
                                      related_name='statictic_entrance_page')
    exit_page = models.ForeignKey('siteeditor.Block', verbose_name=_('Exit page'),
                                  related_name='statistic_exit_page')
    search_request = models.TextField(verbose_name=_('Search request'))
