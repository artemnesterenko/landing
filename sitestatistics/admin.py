from django.contrib import admin
from .models import *


class StatisticAdmin(admin.ModelAdmin):
    date_hierarchy = 'date'
    pass

admin.site.register(Statistic, StatisticAdmin)