from django.conf.urls import url, include
from .views import *

urlpatterns = [
    url(r'^$', user_profile, name='user_profile'),
    url(r'^(?P<page>[1-9]+\d*)$', user_profile, name='user_profile_page'),
    url(r'^edit$', user_edit),
    url(r'^pay$', user_pay),
    url(r'add_card$', user_add_card)
]
