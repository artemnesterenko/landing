from django.shortcuts import render
from siteeditor.models import Site
from django.core.paginator import Paginator, InvalidPage
from django.contrib.auth.decorators import login_required
from django.utils.translation import ugettext as _


@login_required
def user_profile(request, page=1):
    user = request.user
    greeting = _("Hello, {user_name}!").format(user_name=user.get_username())
    context = {'greeting': greeting}
    sites = Site.objects.filter(user=user)
    sites_on_page = 5
    paginator = Paginator(sites, sites_on_page)
    try:
        context['sites'] = paginator.page(page)
    except InvalidPage:
        context['sites'] = paginator.page(1)
    return render(request, 'billing/profile.html', context)


def user_edit(request, login):
    return render(request, 'billing/edit.html', {'login': login})


def user_pay(request, login):
    return render(request, 'billing/pay.html', {'login': login})


def user_add_card(request, login):
    return render(request, 'billing/add_card.html', {'login': login})


def registration(request):
    return render(request, 'billing/registration.html')
