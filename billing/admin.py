from django.contrib import admin
from django.contrib.auth.admin import UserAdmin
from django.contrib.auth.models import User
from .models import ExtUser, Payment


class ExtUserInline(admin.StackedInline):
    model = ExtUser
    can_delete = False
    verbose_name_plural = 'ExtUserData'


class UserAdmin(UserAdmin):
    inlines = (ExtUserInline, )


class PayementAdmin(admin.ModelAdmin):
    pass


# Re-register UserAdmin
admin.site.unregister(User)
admin.site.register(User, UserAdmin)
admin.site.register(Payment, PayementAdmin)