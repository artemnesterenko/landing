from django.core.validators import RegexValidator

# Ориентировано на российские мобильные + городские с кодом из 3 цифр (например, Москва)
# http://habrahabr.ru/post/110731/
phone_validator = RegexValidator(r'^((8|\+7)[\- ]?)?(\(?\d{3}\)?[\- ]?)?[\d\- ]{7,10}$',
                                 message='Enter correct phone number')
numeric = RegexValidator(r'^[0-9]*$', message='Only numeric characters are allowed.')
alpha = RegexValidator(r'[A-Z]*$', message='Only uppercase latin letters are allowed.')
