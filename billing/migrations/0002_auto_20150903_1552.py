# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import django.core.validators


class Migration(migrations.Migration):

    dependencies = [
        ('billing', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='cards',
            name='expiration_month',
            field=models.PositiveSmallIntegerField(verbose_name='Месяц окончания'),
        ),
        migrations.AlterField(
            model_name='cards',
            name='expiration_year',
            field=models.PositiveSmallIntegerField(verbose_name='Год окончания'),
        ),
        migrations.AlterField(
            model_name='cards',
            name='first_name',
            field=models.CharField(validators=[django.core.validators.RegexValidator('[A-Z]*$', message='Only uppercase latin letters are allowed.')], verbose_name='Имя', max_length=255),
        ),
        migrations.AlterField(
            model_name='cards',
            name='last_name',
            field=models.CharField(validators=[django.core.validators.RegexValidator('[A-Z]*$', message='Only uppercase latin letters are allowed.')], verbose_name='Фамилия', max_length=255),
        ),
        migrations.AlterField(
            model_name='extuser',
            name='phone_number',
            field=models.CharField(validators=[django.core.validators.RegexValidator('^((8|\\+7)[\\- ]?)?(\\(?\\d{3}\\)?[\\- ]?)?[\\d\\- ]{7,10}$', message='Enter correct phone number')], verbose_name='Телефон', max_length=16),
        ),
        migrations.AlterField(
            model_name='extuser',
            name='subscription',
            field=models.ForeignKey(verbose_name='Подписка', to='billing.Subscriptions'),
        ),
        migrations.AlterField(
            model_name='extuser',
            name='subscription_card',
            field=models.ForeignKey(verbose_name='Карта списания', to='billing.Cards'),
        ),
        migrations.AlterField(
            model_name='extuser',
            name='subscription_enabled',
            field=models.BooleanField(default=False, verbose_name='Подписка активна'),
        ),
        migrations.AlterField(
            model_name='extuser',
            name='subscription_expiration',
            field=models.DateTimeField(verbose_name='Истечение подписки'),
        ),
        migrations.AlterField(
            model_name='permissions',
            name='name',
            field=models.CharField(unique=True, max_length=255, verbose_name='Название'),
        ),
        migrations.AlterField(
            model_name='subscriptions',
            name='name',
            field=models.CharField(unique=True, max_length=255, verbose_name='Название'),
        ),
        migrations.AlterField(
            model_name='subscriptions',
            name='permissions',
            field=models.ManyToManyField(verbose_name='Разрешения', to='billing.Permissions'),
        ),
    ]
