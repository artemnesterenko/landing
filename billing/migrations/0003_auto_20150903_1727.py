# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('billing', '0002_auto_20150903_1552'),
    ]

    operations = [
        migrations.CreateModel(
            name='Payment',
            fields=[
                ('id', models.AutoField(primary_key=True, auto_created=True, verbose_name='ID', serialize=False)),
                ('date', models.DateTimeField(auto_now_add=True, verbose_name='Дата')),
                ('amount', models.DecimalField(decimal_places=2, verbose_name='Сумма', max_digits=8)),
                ('subscription', models.ForeignKey(verbose_name='Подписка', to='billing.Subscriptions')),
                ('user', models.ForeignKey(verbose_name='Платильщик', to=settings.AUTH_USER_MODEL)),
            ],
        ),
        migrations.RenameField(
            model_name='cards',
            old_name='owner',
            new_name='user',
        ),
    ]
