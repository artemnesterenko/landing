# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import django.core.validators
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='Cards',
            fields=[
                ('id', models.AutoField(verbose_name='ID', primary_key=True, serialize=False, auto_created=True)),
                ('number', models.CharField(validators=[django.core.validators.RegexValidator('^[0-9]*$', message='Only numeric characters are allowed.')], max_length=16, unique=True)),
                ('first_name', models.CharField(validators=[django.core.validators.RegexValidator('[A-Z]*$', message='Only uppercase latin letters are allowed.')], verbose_name='first name', max_length=255)),
                ('last_name', models.CharField(validators=[django.core.validators.RegexValidator('[A-Z]*$', message='Only uppercase latin letters are allowed.')], verbose_name='last name', max_length=255)),
                ('expiration_year', models.PositiveSmallIntegerField(verbose_name='expiration year')),
                ('expiration_month', models.PositiveSmallIntegerField(verbose_name='expiration month')),
                ('cvv', models.PositiveSmallIntegerField(verbose_name='CVV/CVC')),
            ],
        ),
        migrations.CreateModel(
            name='ExtUser',
            fields=[
                ('id', models.AutoField(verbose_name='ID', primary_key=True, serialize=False, auto_created=True)),
                ('phone_number', models.CharField(validators=[django.core.validators.RegexValidator('^((8|\\+7)[\\- ]?)?(\\(?\\d{3}\\)?[\\- ]?)?[\\d\\- ]{7,10}$', message='Enter correct phone number')], verbose_name='phone number', max_length=16)),
                ('subscription_enabled', models.BooleanField(default=False)),
                ('subscription_expiration', models.DateTimeField(verbose_name='subscription expiration')),
            ],
        ),
        migrations.CreateModel(
            name='Permissions',
            fields=[
                ('id', models.AutoField(verbose_name='ID', primary_key=True, serialize=False, auto_created=True)),
                ('name', models.CharField(verbose_name='name', max_length=255, unique=True)),
            ],
        ),
        migrations.CreateModel(
            name='Subscriptions',
            fields=[
                ('id', models.AutoField(verbose_name='ID', primary_key=True, serialize=False, auto_created=True)),
                ('name', models.CharField(verbose_name='name', max_length=255, unique=True)),
                ('price', models.DecimalField(decimal_places=2, max_digits=8)),
                ('permissions', models.ManyToManyField(to='billing.Permissions')),
            ],
        ),
        migrations.AddField(
            model_name='extuser',
            name='subscription',
            field=models.ForeignKey(to='billing.Subscriptions'),
        ),
        migrations.AddField(
            model_name='extuser',
            name='subscription_card',
            field=models.ForeignKey(to='billing.Cards'),
        ),
        migrations.AddField(
            model_name='extuser',
            name='user',
            field=models.OneToOneField(to=settings.AUTH_USER_MODEL),
        ),
        migrations.AddField(
            model_name='cards',
            name='owner',
            field=models.ForeignKey(to='billing.ExtUser'),
        ),
    ]
