from django.db import models
from django.contrib.auth.models import User
from .validators import *
from django.utils.translation import ugettext_lazy as _
from django.utils.translation import pgettext_lazy


class ExtUser(models.Model):
    user = models.OneToOneField(User)
    phone_number = models.CharField(verbose_name=_('Phone'), max_length=16, validators=[phone_validator])
    subscription = models.ForeignKey('Subscriptions', verbose_name=_('Subscription'))
    subscription_enabled = models.BooleanField(default=False, verbose_name=_('Subscription enabled'))
    subscription_expiration = models.DateTimeField(verbose_name=_('Subscription expiration'))
    subscription_card = models.ForeignKey('Cards', verbose_name=_('Payment card'))

    #country = models.CharField(verbose_name='country', max_length=255, validators=[alpha])
    #preferred_language = models.CharField(verbose_name='preferred language', max_length=255, validators=[alpha])


class Cards(models.Model):
    user = models.ForeignKey('ExtUser')
    number = models.CharField(unique=True, max_length=16, validators=[numeric])
    first_name = models.CharField(verbose_name=_('First name'), max_length=255, validators=[alpha])
    last_name = models.CharField(verbose_name=_('Last name'), max_length=255, validators=[alpha])
    expiration_year = models.PositiveSmallIntegerField(verbose_name=_('Expiration year'))
    expiration_month = models.PositiveSmallIntegerField(verbose_name=_('Expiration month'))
    cvv = models.PositiveSmallIntegerField(verbose_name='CVV/CVC')


class Subscriptions(models.Model):
    name = models.CharField(verbose_name=_('Name'), max_length=255, unique=True)
    price = models.DecimalField(verbose_name=_('Price'), max_digits=8, decimal_places=2)
    permissions = models.ManyToManyField('Permissions', verbose_name=_('Permissions'))


class Permissions(models.Model):
    name = models.CharField(unique=True, max_length=255, verbose_name=_('Name'))


class Payment(models.Model):
    user = models.ForeignKey(User, verbose_name=_('Payer'))
    date = models.DateTimeField(verbose_name=pgettext_lazy('Time', 'Date'), auto_now_add=True)
    subscription = models.ForeignKey('Subscriptions', verbose_name=_('Subscription'))
    amount = models.DecimalField(verbose_name=_('Amount'), max_digits=8, decimal_places=2)
