#!/bin/bash
web_dockerfile=landing_dockerfile
db_name=landing_db
web_name=landing_web
web_image=landing
db_login=postgres
docker rm -f $db_name $web_name
docker run -d --name $db_name postgres
db_ip=$(docker inspect -f {{.NetworkSettings.IPAddress}} $db_name)
docker build -t $web_image --rm=true -f $web_dockerfile .
echo 'Waiting for postgres init'
sleep 20s
docker exec -u $db_login $db_name psql -c 'CREATE DATABASE landing;'
docker exec -u $db_login $db_name psql -c "CREATE USER admin WITH password 'qwerty';"
docker exec -u $db_login $db_name psql -c 'GRANT ALL privileges ON DATABASE landing TO admin;'
docker run -it --name $web_name --link $db_name:db $web_image /bin/bash ./src/init.sh
#web_ip=$(docker inspect -f {{.NetworkSettings.IPAddress}} $web_name)
#echo "Адрес подключения к серверу Django-проекта: $web_ip:8000"
