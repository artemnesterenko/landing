from django.shortcuts import render
from siteeditor.models import Block, Site
from django.contrib.auth.decorators import login_required
from django.core.exceptions import PermissionDenied
from django.http.response import Http404, HttpResponseRedirect, HttpResponseForbidden
from .forms import BlockForm, SiteForm
from django.views.decorators.http import require_POST
from .cheks import check_ownership


@login_required
def add_site(request):
    user = request.user
    header = request.POST.get('header')
    footer = request.POST.get('footer')
    if header is not None and footer is not None:
        site = Site(header=header, footer=footer, user=user)
        site.save()
        context = {'added_site': site}
        return render(request, 'siteeditor/add_site.html', context)
    return render(request, 'siteeditor/add_site.html')


@login_required
def site_editor(request, site_id):
    user = request.user
    if not check_ownership(user, site_id):
        raise PermissionDenied()
    blocks = Block.objects.filter(site=site_id).order_by('order_number')
    context = {}
    next_block_number = blocks.last().order_number + 1 if blocks else 1
    context['block_form'] = BlockForm(user, initial={'site': site_id, 'order_number': next_block_number})
    context['blocks'] = blocks
    context['site'] = Site.objects.get(pk=site_id)
    return render(request, 'siteeditor/editor.html', context)


@login_required
@require_POST
def edit_block(request):
    user = request.user
    site = request.POST.get('site')
    block_form = BlockForm(user, request.POST)
    if block_form.is_valid():
        block_form.save()
    return HttpResponseRedirect("/editor/edit_site/%s" % site)


@login_required
def edit_content(request):
    user = request.user
    block_id = request.POST.get('block')
    block = Block.objects.get(pk=block_id)
    content_form = ContentForm(request.POST)
    if content_form.is_validы():
        content_form.save()
    return HttpResponseRedirect("/editor/edit_site/%s#block-%s" )#% (site.id, block.order_number))
