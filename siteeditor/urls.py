from django.conf.urls import include, url
from .views import add_site, site_editor, edit_block, edit_content

urlpatterns = [
    url(r'^add_site/', add_site, name='add_site'),
    url(r'^edit_site/(?P<site_id>[1-9]+\d*)', site_editor, name='edit_site'),
    url(r'^edit_block/', edit_block, name='edit_block')
    # url(r'^edit_content/', edit_content, name='edit_content')
]
