from .models import Site


def check_ownership(user, site_id):
    is_owns = True
    try:
        site = Site.objects.get(pk=site_id)
        if site.user != user:
            is_owns = False
    except Site.DoesNotExist:
        is_owns = False
    return is_owns
