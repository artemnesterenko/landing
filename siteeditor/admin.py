from django.contrib import admin
from .models import *


class SiteAdmin(admin.ModelAdmin):
    list_filter = ['user', 'header', 'footer']
    list_display = ['user', 'header', 'footer']


class BlockAdmin(admin.ModelAdmin):
    list_display = ['site', 'title', 'order_number']  # 'template',
    pass


class ContentAdmin(admin.ModelAdmin):
    pass


class TemplateAdmin(admin.ModelAdmin):
    pass

# Re-register UserAdmin
#admin.site.unregister(User)
admin.site.register(Site, SiteAdmin)
admin.site.register(Block, BlockAdmin)
admin.site.register(Content, ContentAdmin)
admin.site.register(Template, TemplateAdmin)