from django.forms import ModelForm, HiddenInput
from .models import Block, Content, Site
from django.forms import ValidationError
from django.utils.translation import gettext as _
from .cheks import check_ownership


class BlockForm(ModelForm):
    class Meta:
        model = Block
        fields = ['title', 'content',  'order_number', 'site']  # 'template',
        widgets = {
            'site': HiddenInput(),
            'order_number': HiddenInput()
        }

    def __init__(self, user, *args, **kwargs):
        self.user = user
        super().__init__(*args, **kwargs)

    def clean_site(self):
        site = self.cleaned_data['site']
        if not check_ownership(self.user, site.id):
            raise ValidationError(_('Site is not owned by current user'))
        return site


class SiteForm(ModelForm):
    class Meta:
        model = Site
        fields = ['header', 'footer', 'user']
        widgets = {
            'user': HiddenInput()
        }
