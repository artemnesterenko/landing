from django.db import models
from django.contrib.auth.models import User
from django.utils.translation import ugettext_lazy as _


class Site(models.Model):
    user = models.ForeignKey(User, verbose_name=_('Owner'))
    header = models.TextField(verbose_name='Header')
    footer = models.TextField(verbose_name='Footer')

    def __str__(self):
        return "ID: %s, User: %s" % (self.pk, self.user.get_username())


class Template(models.Model):
    name = models.CharField(verbose_name=_('Name'), max_length=255)
    file = models.FilePathField(verbose_name=_('Path'), path='siteeditor/templates/siteeditor/blocks', default='siteeditor/blocks/table.html')

    def __str__(self):
        return str(self.name)


class Block(models.Model):
    site = models.ForeignKey('Site', verbose_name=_('Site'))
    #template = models.ForeignKey('Template', verbose_name=_('Template'))
    title = models.TextField(verbose_name=_('Header'))
    order_number = models.PositiveSmallIntegerField(verbose_name=_('Order number'), default=1)
    content = models.TextField(verbose_name=_('Content'), null=True)

    def __str__(self):
        return '%s %s' % (str(self.site), self.title)


class Content(models.Model):
    # block = models.ForeignKey('Block', verbose_name=_('Block'))
    body = models.TextField(verbose_name=_('Content'))
    #order_number = models.PositiveSmallIntegerField(verbose_name=_('Order number'), default=1)

    def __str__(self):
        return str(self.body)
